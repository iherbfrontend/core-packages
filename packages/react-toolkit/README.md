# `react-toolkit`

## Usage

Here is a quick example to get started with iHerb react-toolkit:

Installation:

```yarn
yarn add @iherb/react-toolkit

```

```npm
npm install @iherb/react-toolkit

```

## Get Started

react-toolkit components follow material ui style and convention.
We can wrap components in ThemeProvider and use react-toolkit theme object as configuration prop:


```jsx
import { ThemeProvider, themes } from "@iherb/react-toolkit";
import { AppBar } from "@iherb/react-toolkit";

// BackOfficeTheme comes from /react-toolkit/src/themes
function App() {
  return (
  	<ThemeProvider theme={themes.BackOfficeTheme(ENV)}>
	  	<AppBar>Header Content</AppBar>;
  	</ThemePriovider>
  )
}

ReactDOM.render(<App />, document.querySelector('#app'));
```


## Dependencies

In order to install this into your project you will need to login to authenticate with iHerb jFrog:
[https://iherbglobal.atlassian.net/wiki/spaces/TEC/pages/439681379/How+to+use+JFrog+Artifactory+as+NPM+repository](https://iherbglobal.atlassian.net/wiki/spaces/TEC/pages/439681379/How+to+use+JFrog+Artifactory+as+NPM+repository)

or:

```
npm login --registry=https://iherb.jfrog.io/artifactory/api/npm/npm/

```

## Configuring Jenkins

In order to deploy any react-toolkit components you will need jenkins to also authenticate with jFrog:
You can create an .npmrc file with jFrog credentials:

.npmrc
```
@iherb:registry=https://iherb.jfrog.io/artifactory/api/npm/npm/
//iherb.jfrog.io/artifactory/api/npm/npm/:_authToken={YourAuthToken}
email=rick.chen@iherb.com
registry=https://iherb.jfrog.io/artifactory/api/npm/npm/
always-auth=true

```

Copy it over in build Dockerfile
```
COPY .npmrc .npmrc

```

