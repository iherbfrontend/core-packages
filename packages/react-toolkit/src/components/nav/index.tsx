import React from 'react';
import PropTypes from 'prop-types'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography'
import Link from '@material-ui/core/Link';
import clsx from 'clsx'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    toolbarRoot: {
      alignItems: 'normal',
      minHeight: 40,
    },
    linkContainer: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      '&:not(:last-child)': {
         marginRight: 16, 
      },
      '&:not(:first-child)': {
         marginLeft: 16, 
      },
    },
    active: {
      borderBottom: `solid 2px ${theme.palette.primary.main}`,
    },
    link: {
      '&:hover': {
        textDecoration: 'none',
      }
    },
}))

const Nav = ({ navLinks }) => {
  const classes = useStyles()
  return (
      <Toolbar classes={{root: classes.toolbarRoot}}>
          {navLinks.map((navLink, i) => {
             return ( 
              <div className={clsx(classes.linkContainer, navLink.active && classes.active)} key={i}>
                <div><Link className={classes.link} color='inherit' href={navLink.path}>
                  <Typography color={navLink.active ? 'primary' : 'textPrimary'}>{navLink.title}</Typography>
                </Link>
                </div>
              </div>
              )
          })}
      </Toolbar>
  )
}

Nav.propTypes = {
  navLinks: PropTypes.arrayOf(
    PropTypes.shape({
      path: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      active: PropTypes.bool,
    })
  ).isRequired
}

export default Nav