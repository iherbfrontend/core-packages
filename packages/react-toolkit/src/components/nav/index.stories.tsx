import React, { useState } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import random from 'lodash/random'
import clone from 'lodash/clone'
import Icon from '@material-ui/core/Icon'
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Button from '@material-ui/core/Button';
import Nav from './index';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  }),
);

export default {
  title: 'Nav'
}

export const MenuAppBar = () => {
  const classes = useStyles();
  const [navLinks, setNavLinks] = useState(
    [
      {
        title: 'Pricing Approval',
        path: '/approval',
        active: true,
      },
      {
        title: 'Batch Upload',
        path: '/batch-upload',
        active: false,
      },
      {
        title: 'Product Pricing',
        path: '/pricing',
        active: false,
      },
      {
        title: 'Pricing Rules',
        path: '/rules',
        active: false,
      },
      {
        title: 'Markup Settings',
        path: '/markup-settings',
        active: false,
      },
    ] 
  )

  const handleButtonClick = () => {
    const randomNumber = random(0, 4)
    const cloneLinks = clone(navLinks)
    const oldActive = cloneLinks.find(navLink => navLink.active)
    oldActive.active = false
    cloneLinks[randomNumber].active = true
    setNavLinks(cloneLinks)
  }

    console.log('navlink', navLinks)
  return (
    <div className={classes.root}>
      <Button onClick={handleButtonClick}>Go To Random Link</Button>
      <Nav navLinks={navLinks}/>
    </div>
  );
}
