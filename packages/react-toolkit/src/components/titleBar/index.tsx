import React from 'react';
import PropTypes from 'prop-types'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

import AppBar from '../appBar'
import Nav from '../nav'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    divider: {
      marginTop: 12,
    } 
  }),
);

const TitleBar = ({ title, auth, navLinks }) => {
  const classes = useStyles()
    return (
        <>
        <AppBar title={title} auth={auth}/>
        <div className={classes.divider}></div>
        <Nav navLinks={navLinks}/>
        </>
    )
}

TitleBar.propTypes = {
    title: PropTypes.shape({
      text: PropTypes.string.isRequired,
      path: PropTypes.string,
    }),
    auth: PropTypes.shape({
        username: PropTypes.string,
        onLoginClick: PropTypes.func.isRequired,
        onLogoutClick: PropTypes.func.isRequired,
      }).isRequired,
      navLinks: PropTypes.arrayOf(PropTypes.shape({
        path: PropTypes.string,
        title: PropTypes.string,
        active: PropTypes.bool, 
      })
      )

}

export default TitleBar