import React from 'react'
import { TextField as MaterialUITextField, TextFieldProps } from '@material-ui/core';

type Props = TextFieldProps 

const TextField = (props: Props) => (
    <MaterialUITextField { ...props} />
)

export default TextField
