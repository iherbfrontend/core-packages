import React from 'react'
import { Button as MaterialUIButton, ButtonProps } from '@material-ui/core';

type Props = ButtonProps

const Button = (props: Props) => (
    <MaterialUIButton { ...props} />
)

export default Button
