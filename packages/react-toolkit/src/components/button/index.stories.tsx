import React from "react";
import Button from './index';

export default {
  title: "Button"
};

export const Primary = () => (
    <Button variant="contained" color="primary" >Primary </Button>
)
