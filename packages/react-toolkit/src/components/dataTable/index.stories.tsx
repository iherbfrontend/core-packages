import React from 'react'
import DataTable from './index'

const BasicGrid = () => {
  return (
    <div style={{ maxWidth: '100%' }}>
        <DataTable
          columns={[
            { title: 'Name', field: 'name' },
            { title: 'Surname', field: 'surname' },
            { title: 'Birth Year', field: 'birthYear', type: 'numeric' },
            {
              title: 'Birth Place',
              field: 'birthCity',
              lookup: { 34: 'İstanbul', 63: 'Şanlıurfa', 21: 'Scranton', 10: 'Philphiladelphia' },
            },
          ]}
          data={[
            { name: 'Mehmet', surname: 'Baran', birthYear: 1987, birthCity: 63 },
            { name: 'Zerya Betül', surname: 'Baran', birthYear: 2017, birthCity: 34 },
            { name: 'Stanley', surname: 'Hudson', birthYear: 2012, birthCity: 10 },
            { name: 'Ryan', surname: 'Howard', birthYear: 2007, birthCity: 21 },
          ]}
          title="Demo Title"
        />
  </div>
  )
}

export {
    BasicGrid
}

export default {
    title: "DataTable"
}