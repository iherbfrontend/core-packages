import React from 'react'
import { AddBox, Check, Clear, DeleteOutline, ChevronRight, Edit, SaveAlt, FilterList, FirstPage, LastPage, ChevronLeft, Search, ArrowDownward, Remove, ViewColumn } from '@material-ui/icons'
import MaterialTable, { MaterialTableProps, MTableAction, MTableActions, MTableBody, MTableBodyRow, MTableCell, MTableEditField, MTableEditRow, MTableFilterRow, MTableGroupRow, MTableGroupbar, MTableHeader, MTablePagination, MTableToolbar } from '@material-table/core'
import { forwardRef } from 'react'

type Props = MaterialTableProps<Record<string, unknown>>
type Ref = SVGSVGElement 

const tableIcons = {
  Add: forwardRef<Ref, Props>((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef<Ref, Props>((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef<Ref, Props>((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef<Ref, Props>((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef<Ref, Props>((props, ref) => <ChevronRight {...props} ref={ref} />),
  Edit: forwardRef<Ref, Props>((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef<Ref, Props>((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef<Ref, Props>((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef<Ref, Props>((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef<Ref, Props>((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef<Ref, Props>((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef<Ref, Props>((props, ref) => <ChevronLeft {...props} ref={ref} />),
  ResetSearch: forwardRef<Ref, Props>((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef<Ref, Props>((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef<Ref, Props>((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef<Ref, Props>((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef<Ref, Props>((props, ref) => <ViewColumn {...props} ref={ref} />)
}


const DataTable = (props: Props) => (
  <MaterialTable
    {...props}
    icons={tableIcons}/>  
)

DataTable.MTableAction = MTableAction
DataTable.MTableActions = MTableActions
DataTable.MTableBody = MTableBody
DataTable.MTableBodyRow = MTableBodyRow
DataTable.MTableCell = MTableCell
DataTable.MTableEditField = MTableEditField
DataTable.MTableEditRow = MTableEditRow
DataTable.MTableFilterRow = MTableFilterRow
DataTable.MTableGroupRow = MTableGroupRow
DataTable.MTableGroupbar = MTableGroupbar
DataTable.MTableHeader = MTableHeader
DataTable.MTablePagination = MTablePagination
DataTable.MTableToolbar = MTableToolbar

export default DataTable