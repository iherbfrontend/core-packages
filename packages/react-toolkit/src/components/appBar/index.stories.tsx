import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Icon from '@material-ui/core/Icon'
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import AppBar from './index';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  }),
);


export default {
  title: 'AppBar'
}

export const MenuAppBar = () => {
  const classes = useStyles();
  const [auth, setAuth] = React.useState(true);
  const [username, setUsername] = React.useState('joedoe');

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      setUsername('joedoe')
    } 
    else {
      setUsername('')
    }
    setAuth(event.target.checked)
  };

  const onLoginCallback = () => {
    alert('log in')
  }

const onLogoutCallback = () => {
    alert('log out')
  }

  return (
    <div className={classes.root}>
      <FormGroup>
        <FormControlLabel
          control={<Switch checked={auth} onChange={handleChange} aria-label="login switch" />}
          label={auth ? 'Login' : 'Logout'}
        />
      </FormGroup>
      <AppBar title={{text: 'Pricing Portal', path: '/' }} auth={{username: username, onLoginClick: onLoginCallback , onLogoutClick: onLogoutCallback}}/>
    </div>
  );
}
