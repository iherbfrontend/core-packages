import React from 'react';
import PropTypes from 'prop-types'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Icon from '@material-ui/core/Icon'
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import MaterialUIAppBar from '@material-ui/core/Appbar';
import IHerbLogo from '../../assets/iherb_logo.svg';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    logoContainer: {
      cursor: 'pointer',
    },
    logo: {
      width: 75,
      height: 28,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
    appTitle: {
      paddingLeft: 10,
      flexGrow: 1,
      position: 'relative',
      '&::before': {
        content: '""',
        position: 'absolute',
        left: 0,
        top: 0,
        borderRight: '2px solid #FFFFFF',
        transform: 'scaleX(0.5)',
        height: '100%',
      },
    },
    username: {
      paddingRight: 10,
    },
    loginUrl: {
      cursor: 'pointer',
      textDecoration: 'underline',
    },
    logoutUrl: {
      cursor: 'pointer',
      textDecoration: 'underline',
      paddingLeft: 10,
      position: 'relative',
      '&::before': {
        content: '""',
        position: 'absolute',
        left: 0,
        top: 0,
        borderRight: '2px solid #FFFFFF',
        transform: 'scaleX(0.5)',
        height: '100%',
      },
    }
  }),
);

const AppBar = ({ title, auth }) => {
  const classes = useStyles();

  const onLoginClick = (event: React.SyntheticEvent) => {
    event.preventDefault()
    auth.onLoginClick()
  }

  const onLogoutClick = (event: React.SyntheticEvent) => {
    event.preventDefault()
    auth.onLogoutClick()
  }

  const onLogoClick = (event: React.SyntheticEvent) => {
    event.preventDefault()
    window.location = title.path
  }

  return (
    <div className={classes.root}>
      <MaterialUIAppBar position="static">
        <Toolbar>
          <div className={classes.logoContainer} onClick={onLogoClick}><Icon classes={{root: classes.logo}}><img src={IHerbLogo}/></Icon></div>
          <div className={classes.appTitle}><Typography>{title.text}</Typography></div>
          {auth.username && (
            <>
            <div className={classes.username}>{auth.username}</div>
            <div className={classes.logoutUrl}><Link onClick={onLogoutClick} color='inherit'>Logout</Link></div>
            </>
          )}
          {!auth.username && (
            <div className={classes.loginUrl}><Link onClick={onLoginClick} color='inherit'>Login</Link></div>
          )}
        </Toolbar>
      </MaterialUIAppBar>
    </div>
  );
}

AppBar.propTypes = {
  title: PropTypes.shape({
    text: PropTypes.string.isRequired,
    path: PropTypes.string,
  }),
  auth: PropTypes.shape({
    username: PropTypes.string,
    onLoginClick: PropTypes.func.isRequired,
    onLogoutClick: PropTypes.func.isRequired,
  }).isRequired
}

export default AppBar