import { createTheme } from '@material-ui/core/styles'
import { ENVIRONMENT } from '@iherb/constants'
import { getPrimaryColor } from '../utils/color'

const backOfficeTheme = (env = ENVIRONMENT.TEST) => {
  const primaryColor = getPrimaryColor(env)
  return createTheme({
    palette: {
      primary: {
        main: primaryColor,
      }
    },
    // overrides: {
    //   MuiAppBar: {
    //     colorPrimary: {
    //       backgroundColor: '#ff0000'
    //     }
    //   }
    // }
  })
}

export default backOfficeTheme