const primaryGreen = '#458500'
const primaryBlue = '#1976D2'
const primaryOrange = '#F38A00'

export {
    primaryGreen,
    primaryBlue,
    primaryOrange,
}