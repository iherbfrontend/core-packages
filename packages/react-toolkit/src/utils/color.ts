import { primaryGreen, primaryBlue, primaryOrange} from '../themes/colors'
type environments = 'local' | 'test' | 'preproduction' | 'production'
const getPrimaryColor = (env: environments)  => {
    switch (env) {
        case 'local':
        case 'test':
            return primaryBlue;
        case 'preproduction':
            return primaryOrange;
        case 'production':
            return primaryGreen;
        default:
            return primaryBlue;
    }
}

export {
    getPrimaryColor
}