import  { CountryCodes} from './country-codes'
import  { CurrencyCodes }  from './currency-codes'
import { LanguageCodes }  from './language-codes'

export {
  CountryCodes,
  CurrencyCodes,
  LanguageCodes
}

export enum WeightPreference {
  Pounds = 1,
  Kilograms = 2,
}

export enum CurrentPlatform {
  Desktop = 0,
  Mobile = 1,
}

export enum OrderingStore {
  iHerb = 0,
  Lumina = 1,
}

/**
 * Definition of data status
 * init means initialize
 * pending means data is loading
 * fulfilled means data return successfully
 * rejected means data return unsuccessfully
 */
export enum DataStatus {
  init = 'init',
  pending = 'pending',
  fulfilled = 'fulfilled',
  rejected = 'rejected',
}

export interface Pref {
  crs: CurrentPlatform;
  lan: LanguageCodes;
  sccode: CountryCodes;
  scurcode: CurrencyCodes;
  storeid: OrderingStore;
  wp: WeightPreference;
}
