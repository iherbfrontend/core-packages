import { parse } from 'querystring';
import nookies from 'nookies';
import { COOKIES } from '@iherb-checkout/base/constants';
import { CurrentPlatform, Pref } from '@iherb-fe/types';
import { getDefaultPrefByPlatform } from '..';
import { isArray, isString } from '../is';

export const getClientCookie = (cname: string): string => {
  const name = `${cname}=`;
  const ca = document.cookie.split(';');
  for (let i = 0; i < ca.length; i += 1) {
    let c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return decodeURIComponent(c.substring(name.length, c.length));
    }
  }
  return '';
};

export const getClientCookieToJSON = (cname: string): { [key: string]: string } => {
  const cookie = getClientCookie(cname);

  if (cookie && cookie !== '') {
    const cookieMap: { [key: string]: string } = {};
    cookie.split('&').forEach((str) => {
      const vals = str.split('=');
      if (vals.length > 1) {
        if (vals[0]) {
          ({ 1: cookieMap[vals[0]] } = vals);
        }
      } else {
        ({ 0: cookieMap.key } = vals);
      }
    });

    return cookieMap;
  }
  return {};
};

export const getNextCookie = (cname: string, ctx: Parameters<typeof nookies.get>[0]): string => nookies.get(ctx)[cname];

export const getNextCookieToJSON = (cname: string, ctx: Parameters<typeof getNextCookie>[1]): Record<string, string | string[]> => {
  const value = getNextCookie(cname, ctx);

  if (!value) {
    return {};
  }

  return { ...parse(value) };
};


// TODO: Temporary disable this method prevent from misusing.
// export const getPrefFromClientCookie = (platform: CurrentPlatform): Pref => {
//   const pref = getClientCookieToJSON(COOKIES.persistent);

//   const defaultPref = getDefaultPrefByPlatform(platform);

//   let newStoreId = defaultPref.storeid;
//   let newWp = defaultPref.wp;

//   const numberStoreId = parseInt(pref.storeid, 10);

//   if (!Number.isNaN(numberStoreId)) {
//     newStoreId = numberStoreId;
//   }

//   const numberWp = parseInt(pref.wp, 10);

//   if (!Number.isNaN(numberWp)) {
//     newWp = numberWp;
//   }

//   return {
//     ...defaultPref,
//     ...pref,
//     storeid: newStoreId,
//     wp: newWp,
//   };
// };

export const getPrefFromNextCookie = (platform: CurrentPlatform, ctx: Parameters<typeof getNextCookie>[1]): Pref => {
  const pref = getNextCookieToJSON(COOKIES.persistent, ctx);

  const defaultPref = getDefaultPrefByPlatform(platform);

  let newStoreId = defaultPref.storeid;
  let newWp = defaultPref.wp;

  const numberStoreId = isString(pref.storeid) ? parseInt(pref.storeid, 10) : defaultPref.storeid;

  if (!Number.isNaN(numberStoreId)) {
    newStoreId = numberStoreId;
  }

  const numberWp = isString(pref.wp) ? parseInt(pref.wp, 10) : defaultPref.wp;

  if (!Number.isNaN(numberWp)) {
    newWp = numberWp;
  }

  return {
    ...defaultPref,
    ...pref,
    storeid: newStoreId,
    wp: newWp,
  };
};

