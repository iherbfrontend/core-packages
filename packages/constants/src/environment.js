const TEST = 'test'
const LOCAL = 'local'
const PRODUCTION = 'production'
const PREPRODUCTION = 'preprodution'

export {
    TEST,
    LOCAL,
    PRODUCTION,
    PREPRODUCTION,
}